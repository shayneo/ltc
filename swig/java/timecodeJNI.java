/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.shaynemachine3000.timecode;

public class timecodeJNI {
  public final static native int LTC_H_get();
  public final static native String LIBLTC_VERSION_get();
  public final static native int LIBLTC_VERSION_MAJOR_get();
  public final static native int LIBLTC_VERSION_MINOR_get();
  public final static native int LIBLTC_VERSION_MICRO_get();
  public final static native int LIBLTC_CUR_get();
  public final static native int LIBLTC_REV_get();
  public final static native int LIBLTC_AGE_get();
  public final static native int LTC_FRAME_BIT_COUNT_get();
  public final static native void LTCFrame_frame_units_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_frame_units_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user1_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user1_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_frame_tens_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_frame_tens_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_dfbit_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_dfbit_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_col_frame_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_col_frame_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user2_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user2_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_secs_units_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_secs_units_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user3_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user3_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_secs_tens_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_secs_tens_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_biphase_mark_phase_correction_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_biphase_mark_phase_correction_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user4_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user4_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_mins_units_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_mins_units_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user5_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user5_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_mins_tens_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_mins_tens_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_binary_group_flag_bit0_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_binary_group_flag_bit0_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user6_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user6_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_hours_units_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_hours_units_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user7_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user7_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_hours_tens_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_hours_tens_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_binary_group_flag_bit1_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_binary_group_flag_bit1_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_binary_group_flag_bit2_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_binary_group_flag_bit2_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_user8_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_user8_get(long jarg1, LTCFrame jarg1_);
  public final static native void LTCFrame_sync_word_set(long jarg1, LTCFrame jarg1_, long jarg2);
  public final static native long LTCFrame_sync_word_get(long jarg1, LTCFrame jarg1_);
  public final static native long new_LTCFrame();
  public final static native void delete_LTCFrame(long jarg1);
  public final static native int LTC_USE_DATE_get();
  public final static native int LTC_TC_CLOCK_get();
  public final static native int LTC_BGF_DONT_TOUCH_get();
  public final static native int LTC_NO_PARITY_get();
  public final static native void LTCFrameExt_ltc_set(long jarg1, LTCFrameExt jarg1_, long jarg2, LTCFrame jarg2_);
  public final static native long LTCFrameExt_ltc_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_off_start_set(long jarg1, LTCFrameExt jarg1_, long jarg2);
  public final static native long LTCFrameExt_off_start_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_off_end_set(long jarg1, LTCFrameExt jarg1_, long jarg2);
  public final static native long LTCFrameExt_off_end_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_reverse_set(long jarg1, LTCFrameExt jarg1_, int jarg2);
  public final static native int LTCFrameExt_reverse_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_biphase_tics_set(long jarg1, LTCFrameExt jarg1_, long jarg2);
  public final static native long LTCFrameExt_biphase_tics_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_sample_min_set(long jarg1, LTCFrameExt jarg1_, short jarg2);
  public final static native short LTCFrameExt_sample_min_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_sample_max_set(long jarg1, LTCFrameExt jarg1_, short jarg2);
  public final static native short LTCFrameExt_sample_max_get(long jarg1, LTCFrameExt jarg1_);
  public final static native void LTCFrameExt_volume_set(long jarg1, LTCFrameExt jarg1_, double jarg2);
  public final static native double LTCFrameExt_volume_get(long jarg1, LTCFrameExt jarg1_);
  public final static native long new_LTCFrameExt();
  public final static native void delete_LTCFrameExt(long jarg1);
  public final static native void SMPTETimecode_timezone_set(long jarg1, SMPTETimecode jarg1_, String jarg2);
  public final static native String SMPTETimecode_timezone_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_years_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_years_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_months_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_months_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_days_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_days_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_hours_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_hours_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_mins_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_mins_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_secs_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_secs_get(long jarg1, SMPTETimecode jarg1_);
  public final static native void SMPTETimecode_frame_set(long jarg1, SMPTETimecode jarg1_, short jarg2);
  public final static native short SMPTETimecode_frame_get(long jarg1, SMPTETimecode jarg1_);
  public final static native long new_SMPTETimecode();
  public final static native void delete_SMPTETimecode(long jarg1);
  public final static native void ltc_frame_to_time(long jarg1, SMPTETimecode jarg1_, long jarg2, LTCFrame jarg2_, int jarg3);
  public final static native void ltc_time_to_frame(long jarg1, LTCFrame jarg1_, long jarg2, SMPTETimecode jarg2_, int jarg3, int jarg4);
  public final static native void ltc_frame_reset(long jarg1, LTCFrame jarg1_);
  public final static native int ltc_frame_increment(long jarg1, LTCFrame jarg1_, int jarg2, int jarg3, int jarg4);
  public final static native int ltc_frame_decrement(long jarg1, LTCFrame jarg1_, int jarg2, int jarg3, int jarg4);
  public final static native long ltc_decoder_create(int jarg1, int jarg2);
  public final static native int ltc_decoder_free(long jarg1);
  public final static native void ltc_decoder_write(long jarg1, long jarg2, long jarg3, long jarg4);
  public final static native void ltc_decoder_write_float(long jarg1, long jarg2, long jarg3, long jarg4);
  public final static native void ltc_decoder_write_s16(long jarg1, long jarg2, long jarg3, long jarg4);
  public final static native void ltc_decoder_write_u16(long jarg1, long jarg2, long jarg3, long jarg4);
  public final static native int ltc_decoder_read(long jarg1, long jarg2, LTCFrameExt jarg2_);
  public final static native void ltc_decoder_queue_flush(long jarg1);
  public final static native int ltc_decoder_queue_length(long jarg1);
  public final static native long ltc_encoder_create(double jarg1, double jarg2, int jarg3, int jarg4);
  public final static native void ltc_encoder_free(long jarg1);
  public final static native void ltc_encoder_set_timecode(long jarg1, long jarg2, SMPTETimecode jarg2_);
  public final static native void ltc_encoder_get_timecode(long jarg1, long jarg2, SMPTETimecode jarg2_);
  public final static native int ltc_encoder_inc_timecode(long jarg1);
  public final static native int ltc_encoder_dec_timecode(long jarg1);
  public final static native void ltc_encoder_set_frame(long jarg1, long jarg2, LTCFrame jarg2_);
  public final static native void ltc_encoder_get_frame(long jarg1, long jarg2, LTCFrame jarg2_);
  public final static native int ltc_encoder_get_buffer(long jarg1, long jarg2);
  public final static native long ltc_encoder_get_bufptr(long jarg1, long jarg2, int jarg3);
  public final static native void ltc_encoder_buffer_flush(long jarg1);
  public final static native long ltc_encoder_get_buffersize(long jarg1);
  public final static native int ltc_encoder_reinit(long jarg1, double jarg2, double jarg3, int jarg4, int jarg5);
  public final static native void ltc_encoder_reset(long jarg1);
  public final static native int ltc_encoder_set_bufsize(long jarg1, double jarg2, double jarg3);
  public final static native int ltc_encoder_set_volume(long jarg1, double jarg2);
  public final static native void ltc_encoder_set_filter(long jarg1, double jarg2);
  public final static native int ltc_encoder_encode_byte(long jarg1, int jarg2, double jarg3);
  public final static native void ltc_encoder_encode_frame(long jarg1);
  public final static native void ltc_frame_set_parity(long jarg1, LTCFrame jarg1_, int jarg2);
  public final static native int parse_bcg_flags(long jarg1, LTCFrame jarg1_, int jarg2);
  public final static native long ltc_frame_alignment(double jarg1, int jarg2);
}
