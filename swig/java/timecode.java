/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.shaynemachine3000.timecode;

public class timecode implements timecodeConstants {
  public static void ltc_frame_to_time(SMPTETimecode stime, LTCFrame frame, int flags) {
    timecodeJNI.ltc_frame_to_time(SMPTETimecode.getCPtr(stime), stime, LTCFrame.getCPtr(frame), frame, flags);
  }

  public static void ltc_time_to_frame(LTCFrame frame, SMPTETimecode stime, LTC_TV_STANDARD standard, int flags) {
    timecodeJNI.ltc_time_to_frame(LTCFrame.getCPtr(frame), frame, SMPTETimecode.getCPtr(stime), stime, standard.swigValue(), flags);
  }

  public static void ltc_frame_reset(LTCFrame frame) {
    timecodeJNI.ltc_frame_reset(LTCFrame.getCPtr(frame), frame);
  }

  public static int ltc_frame_increment(LTCFrame frame, int fps, LTC_TV_STANDARD standard, int flags) {
    return timecodeJNI.ltc_frame_increment(LTCFrame.getCPtr(frame), frame, fps, standard.swigValue(), flags);
  }

  public static int ltc_frame_decrement(LTCFrame frame, int fps, LTC_TV_STANDARD standard, int flags) {
    return timecodeJNI.ltc_frame_decrement(LTCFrame.getCPtr(frame), frame, fps, standard.swigValue(), flags);
  }

  public static SWIGTYPE_p_LTCDecoder ltc_decoder_create(int apv, int queue_size) {
    long cPtr = timecodeJNI.ltc_decoder_create(apv, queue_size);
    return (cPtr == 0) ? null : new SWIGTYPE_p_LTCDecoder(cPtr, false);
  }

  public static int ltc_decoder_free(SWIGTYPE_p_LTCDecoder d) {
    return timecodeJNI.ltc_decoder_free(SWIGTYPE_p_LTCDecoder.getCPtr(d));
  }

  public static void ltc_decoder_write(SWIGTYPE_p_LTCDecoder d, SWIGTYPE_p_unsigned_char buf, long size, long posinfo) {
    timecodeJNI.ltc_decoder_write(SWIGTYPE_p_LTCDecoder.getCPtr(d), SWIGTYPE_p_unsigned_char.getCPtr(buf), size, posinfo);
  }

  public static void ltc_decoder_write_float(SWIGTYPE_p_LTCDecoder d, SWIGTYPE_p_float buf, long size, long posinfo) {
    timecodeJNI.ltc_decoder_write_float(SWIGTYPE_p_LTCDecoder.getCPtr(d), SWIGTYPE_p_float.getCPtr(buf), size, posinfo);
  }

  public static void ltc_decoder_write_s16(SWIGTYPE_p_LTCDecoder d, SWIGTYPE_p_short buf, long size, long posinfo) {
    timecodeJNI.ltc_decoder_write_s16(SWIGTYPE_p_LTCDecoder.getCPtr(d), SWIGTYPE_p_short.getCPtr(buf), size, posinfo);
  }

  public static void ltc_decoder_write_u16(SWIGTYPE_p_LTCDecoder d, SWIGTYPE_p_unsigned_short buf, long size, long posinfo) {
    timecodeJNI.ltc_decoder_write_u16(SWIGTYPE_p_LTCDecoder.getCPtr(d), SWIGTYPE_p_unsigned_short.getCPtr(buf), size, posinfo);
  }

  public static int ltc_decoder_read(SWIGTYPE_p_LTCDecoder d, LTCFrameExt frame) {
    return timecodeJNI.ltc_decoder_read(SWIGTYPE_p_LTCDecoder.getCPtr(d), LTCFrameExt.getCPtr(frame), frame);
  }

  public static void ltc_decoder_queue_flush(SWIGTYPE_p_LTCDecoder d) {
    timecodeJNI.ltc_decoder_queue_flush(SWIGTYPE_p_LTCDecoder.getCPtr(d));
  }

  public static int ltc_decoder_queue_length(SWIGTYPE_p_LTCDecoder d) {
    return timecodeJNI.ltc_decoder_queue_length(SWIGTYPE_p_LTCDecoder.getCPtr(d));
  }

  public static SWIGTYPE_p_LTCEncoder ltc_encoder_create(double sample_rate, double fps, LTC_TV_STANDARD standard, int flags) {
    long cPtr = timecodeJNI.ltc_encoder_create(sample_rate, fps, standard.swigValue(), flags);
    return (cPtr == 0) ? null : new SWIGTYPE_p_LTCEncoder(cPtr, false);
  }

  public static void ltc_encoder_free(SWIGTYPE_p_LTCEncoder e) {
    timecodeJNI.ltc_encoder_free(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static void ltc_encoder_set_timecode(SWIGTYPE_p_LTCEncoder e, SMPTETimecode t) {
    timecodeJNI.ltc_encoder_set_timecode(SWIGTYPE_p_LTCEncoder.getCPtr(e), SMPTETimecode.getCPtr(t), t);
  }

  public static void ltc_encoder_get_timecode(SWIGTYPE_p_LTCEncoder e, SMPTETimecode t) {
    timecodeJNI.ltc_encoder_get_timecode(SWIGTYPE_p_LTCEncoder.getCPtr(e), SMPTETimecode.getCPtr(t), t);
  }

  public static int ltc_encoder_inc_timecode(SWIGTYPE_p_LTCEncoder e) {
    return timecodeJNI.ltc_encoder_inc_timecode(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static int ltc_encoder_dec_timecode(SWIGTYPE_p_LTCEncoder e) {
    return timecodeJNI.ltc_encoder_dec_timecode(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static void ltc_encoder_set_frame(SWIGTYPE_p_LTCEncoder e, LTCFrame f) {
    timecodeJNI.ltc_encoder_set_frame(SWIGTYPE_p_LTCEncoder.getCPtr(e), LTCFrame.getCPtr(f), f);
  }

  public static void ltc_encoder_get_frame(SWIGTYPE_p_LTCEncoder e, LTCFrame f) {
    timecodeJNI.ltc_encoder_get_frame(SWIGTYPE_p_LTCEncoder.getCPtr(e), LTCFrame.getCPtr(f), f);
  }

  public static int ltc_encoder_get_buffer(SWIGTYPE_p_LTCEncoder e, SWIGTYPE_p_unsigned_char buf) {
    return timecodeJNI.ltc_encoder_get_buffer(SWIGTYPE_p_LTCEncoder.getCPtr(e), SWIGTYPE_p_unsigned_char.getCPtr(buf));
  }

  public static SWIGTYPE_p_unsigned_char ltc_encoder_get_bufptr(SWIGTYPE_p_LTCEncoder e, SWIGTYPE_p_int size, int flush) {
    long cPtr = timecodeJNI.ltc_encoder_get_bufptr(SWIGTYPE_p_LTCEncoder.getCPtr(e), SWIGTYPE_p_int.getCPtr(size), flush);
    return (cPtr == 0) ? null : new SWIGTYPE_p_unsigned_char(cPtr, false);
  }

  public static void ltc_encoder_buffer_flush(SWIGTYPE_p_LTCEncoder e) {
    timecodeJNI.ltc_encoder_buffer_flush(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static long ltc_encoder_get_buffersize(SWIGTYPE_p_LTCEncoder e) {
    return timecodeJNI.ltc_encoder_get_buffersize(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static int ltc_encoder_reinit(SWIGTYPE_p_LTCEncoder e, double sample_rate, double fps, LTC_TV_STANDARD standard, int flags) {
    return timecodeJNI.ltc_encoder_reinit(SWIGTYPE_p_LTCEncoder.getCPtr(e), sample_rate, fps, standard.swigValue(), flags);
  }

  public static void ltc_encoder_reset(SWIGTYPE_p_LTCEncoder e) {
    timecodeJNI.ltc_encoder_reset(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static int ltc_encoder_set_bufsize(SWIGTYPE_p_LTCEncoder e, double sample_rate, double fps) {
    return timecodeJNI.ltc_encoder_set_bufsize(SWIGTYPE_p_LTCEncoder.getCPtr(e), sample_rate, fps);
  }

  public static int ltc_encoder_set_volume(SWIGTYPE_p_LTCEncoder e, double dBFS) {
    return timecodeJNI.ltc_encoder_set_volume(SWIGTYPE_p_LTCEncoder.getCPtr(e), dBFS);
  }

  public static void ltc_encoder_set_filter(SWIGTYPE_p_LTCEncoder e, double rise_time) {
    timecodeJNI.ltc_encoder_set_filter(SWIGTYPE_p_LTCEncoder.getCPtr(e), rise_time);
  }

  public static int ltc_encoder_encode_byte(SWIGTYPE_p_LTCEncoder e, int arg1, double speed) {
    return timecodeJNI.ltc_encoder_encode_byte(SWIGTYPE_p_LTCEncoder.getCPtr(e), arg1, speed);
  }

  public static void ltc_encoder_encode_frame(SWIGTYPE_p_LTCEncoder e) {
    timecodeJNI.ltc_encoder_encode_frame(SWIGTYPE_p_LTCEncoder.getCPtr(e));
  }

  public static void ltc_frame_set_parity(LTCFrame frame, LTC_TV_STANDARD standard) {
    timecodeJNI.ltc_frame_set_parity(LTCFrame.getCPtr(frame), frame, standard.swigValue());
  }

  public static int parse_bcg_flags(LTCFrame f, LTC_TV_STANDARD standard) {
    return timecodeJNI.parse_bcg_flags(LTCFrame.getCPtr(f), f, standard.swigValue());
  }

  public static long ltc_frame_alignment(double samples_per_frame, LTC_TV_STANDARD standard) {
    return timecodeJNI.ltc_frame_alignment(samples_per_frame, standard.swigValue());
  }

}
