 %module timecode
 %{
 /* Includes the header in the wrapper code */
 #include "ltc.h"
 %}
 
 /* Parse the header file to generate wrappers */
 %include "ltc.h"
