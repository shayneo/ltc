/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.shaynemachine3000.timecode;

public final class LTC_BG_FLAGS {
  public final static LTC_BG_FLAGS LTC_USE_DATE = new LTC_BG_FLAGS("LTC_USE_DATE", timecodeJNI.LTC_USE_DATE_get());
  public final static LTC_BG_FLAGS LTC_TC_CLOCK = new LTC_BG_FLAGS("LTC_TC_CLOCK", timecodeJNI.LTC_TC_CLOCK_get());
  public final static LTC_BG_FLAGS LTC_BGF_DONT_TOUCH = new LTC_BG_FLAGS("LTC_BGF_DONT_TOUCH", timecodeJNI.LTC_BGF_DONT_TOUCH_get());
  public final static LTC_BG_FLAGS LTC_NO_PARITY = new LTC_BG_FLAGS("LTC_NO_PARITY", timecodeJNI.LTC_NO_PARITY_get());

  public final int swigValue() {
    return swigValue;
  }

  public String toString() {
    return swigName;
  }

  public static LTC_BG_FLAGS swigToEnum(int swigValue) {
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (int i = 0; i < swigValues.length; i++)
      if (swigValues[i].swigValue == swigValue)
        return swigValues[i];
    throw new IllegalArgumentException("No enum " + LTC_BG_FLAGS.class + " with value " + swigValue);
  }

  private LTC_BG_FLAGS(String swigName) {
    this.swigName = swigName;
    this.swigValue = swigNext++;
  }

  private LTC_BG_FLAGS(String swigName, int swigValue) {
    this.swigName = swigName;
    this.swigValue = swigValue;
    swigNext = swigValue+1;
  }

  private LTC_BG_FLAGS(String swigName, LTC_BG_FLAGS swigEnum) {
    this.swigName = swigName;
    this.swigValue = swigEnum.swigValue;
    swigNext = this.swigValue+1;
  }

  private static LTC_BG_FLAGS[] swigValues = { LTC_USE_DATE, LTC_TC_CLOCK, LTC_BGF_DONT_TOUCH, LTC_NO_PARITY };
  private static int swigNext = 0;
  private final int swigValue;
  private final String swigName;
}

