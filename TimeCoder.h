//
// Created by Shayne O'Neill on 21/9/17.
//

#ifndef LTC_TIMECODER_H
#define LTC_TIMECODER_H
#include "ltc.h"

class TimeCoder {

public:
    ltcsnd_sample_t* make_buffer(int size);
    ltcsnd_sample_t* read_file_buffer(int size,char* filename);
    LTCFrameExt dec_read (LTCDecoder* dec);
    void test_decoding_file();

};


int main (int argc, char *argv[]);

#endif //LTC_TIMECODER_H
