%module timecode
%include "typemaps.i"
 %{
 /* Includes the header in the wrapper code */

 typedef unsigned char ltcsnd_sample_t;
 #include "ltc.h"
 ltcsnd_sample_t* make_buffer(int size);
 ltcsnd_sample_t* read_file_buffer(int size,char* filename);
 %}
 
 /* Parse the header file to generate wrappers */
 %include "ltc.h"
typedef unsigned char ltcsnd_sample_t;
 %apply ltcsnd_sample_t *OUTPUT { ltcsnd_sample_t *buf }

%inline %{
ltcsnd_sample_t* make_buffer(int size) {
  ltcsnd_sample_t* buf =(ltcsnd_sample_t*) malloc(size);
  return buf;
}
ltcsnd_sample_t* read_file_buffer(int size,char* filename) {
    ltcsnd_sample_t* buf = make_buffer(size);
    //    FILE *fp = fopen("LTC_00_58_00_00__2mins_2997_ndf.wav","r")
    FILE *fp = fopen(filename, "r");
    if (fp != NULL) {

      fread (buf,sizeof(ltcsnd_sample_t),size,fp);
      fclose(fp);
    }
    return buf;
}
 LTCFrameExt dec_read (LTCDecoder* dec) {
   LTCFrameExt frame;
   ltc_decoder_read(dec,&frame);
   return frame;
}
 %}

