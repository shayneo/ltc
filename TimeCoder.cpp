//
// Created by Shayne O'Neill on 21/9/17.
//

#include "TimeCoder.h"
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
using namespace std;

ltcsnd_sample_t* TimeCoder::make_buffer(int size) {
    ltcsnd_sample_t* buf =(ltcsnd_sample_t*) malloc(size);
    return buf;
}
ltcsnd_sample_t* TimeCoder::read_file_buffer(int size,char* filename) {
    ltcsnd_sample_t* buf = this->make_buffer(size);
    //    FILE *fp = fopen("LTC_00_58_00_00__2mins_2997_ndf.wav","r")
    FILE *fp = fopen(filename, "r");
    if (fp != NULL) {

        fread (buf,sizeof(ltcsnd_sample_t),size,fp);
        fclose(fp);
    }
    return buf;
}
LTCFrameExt TimeCoder::dec_read (LTCDecoder* dec) {
    LTCFrameExt frame;
    ltc_decoder_read(dec,&frame);
    return frame;
}

void TimeCoder::test_decoding_file() {

    std::cout << "=======================================" << std::endl;
    std::cout << "Testing Decoding" << std::endl << std::endl;

    int size = 1024;
    char* filename = "/home/pi/ltc/LTC_00_58_00_00__2mins_2997_ndf.wav";
    ltcsnd_sample_t buf[size];
    FILE *f = fopen(filename, "r");

    if (!f) {
        std::cout << "error opening" << filename << std::endl;
        return ;
    }
    size_t n;
    std::cout << "Reading from " << filename << std::endl;
    int total = 0;
    LTCDecoder* dec = ltc_decoder_create(1920,32);
    LTCFrameExt frame;
    memcpy(&frame,malloc(sizeof(LTCFrameExt)),sizeof(LTCFrameExt));
    //LTCFrameExt frame;
    do {
        n = fread(buf,sizeof(ltcsnd_sample_t), size, f);
        ltc_decoder_write(dec, buf, n, total);
        while (ltc_decoder_read(dec, &frame)) {
            SMPTETimecode stime;

            ltc_frame_to_time(&stime, &frame.ltc, 1);

            printf("%04d-%02d-%02d %s ",
                   ((stime.years < 67) ? 2000 + stime.years : 1900 + stime.years),
                   stime.months,
                   stime.days,
                   stime.timezone
            );

            printf("%02d:%02d:%02d%c%02d | %8lld %8lld%s\n",
                   stime.hours,
                   stime.mins,
                   stime.secs,
                   (frame.ltc.dfbit) ? '.' : ':',
                   stime.frame,
                   frame.off_start,
                   frame.off_end,
                   frame.reverse ? "  R" : ""
            );
        }
    } while (n);



/*
    with open("LTC_00_58_00_00__2mins_2997_ndf.wav","rb") as wav_file:
    data = wav_file.read()
    wav_file.seek(0)
    buffer = wav_file.read(1024)

#
sound = bytearray(1024)
    decoder = timecode.ltc_decoder_create(1920,32)
    total = 0

    timecode._timecode.ltc_decoder_write(decoder,ctypes.addressof(buffer),1024,total)
*/

}


int main (int argc, char *argv[]) {
    TimeCoder* coder = new TimeCoder();
    coder->test_decoding_file();
}
